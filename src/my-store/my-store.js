class MyStore extends Polymer.Element {
  static get is() { return 'my-store'; }

  static get properties() {
    return {
      refresh: {
        value: false,
        observer: '_refresh'
      },
      user: {
        type: Object
      },
      products: {
        type: Array
      },
      category:{
        type: String,
        value: 'MLM1182'
      },
      offset:{
        type: String,
        value: '50'
      },
      limit:{
        type: String,
        value: '50'
      },
      urlBase: {
        type: String,
        value: ''
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      },
      showAlert:{
        value:false,
        notify: true
      }
    };
  }

  _refresh(){
    this.getProducts();
  }

  getProducts(){
    this.showSpinner=true;
    let props = this;
    let url = this.urlBase + "/MLM/search?category="+this.category+"&offset="+this.offset+"&limit="+this.limit;
    let client = new XMLHttpRequest();

    client.open("GET", url, true);
    client.setRequestHeader('Content-type','application/json');

    client.onreadystatechange = function() {
      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        let response = JSON.parse(client.responseText);
        props.products = response.results;

        console.log(client.responseText);
        props.showSpinner=false;

      } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
        console.log(client.responseText);
        props.showSpinner=false;

      } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
        console.log(client.responseText);
        props.showSpinner=false;

      }
    }
    client.send();
  }

}

window.customElements.define(MyStore.is, MyStore);

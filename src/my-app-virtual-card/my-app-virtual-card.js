Polymer.setPassiveTouchGestures(true);

class myAppVirtualCard extends Polymer.Element {
  static get is() { return 'my-app-virtual-card'; }

  static get properties() {
    return {
      user: {
        type: Object
      },
      lastAccess:{
        type: String,
        value: ""
      },
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged',
      },
      urlBase: {
        type: String,
        value: ''
      },
      urlStore: {
        type: String,
        value: ''
      },
      refreshCards: {
        value: false
      },
      refreshMovs: {
        value: false
      },
      refreshRecharge: {
        value: false
      },
      refreshStore: {
        value: false
      },
      internalPage:{
        type: String,
        value: "global",
        observer: '_pageInternalChanged',
      },
      showSpinner: {
        value: true
      },
      routeData: Object,
      subroute: Object,
      // This shouldn't be neccessary, but the Analyzer isn't picking up
      // Polymer.Element#rootPath
      rootPath: String,
      showAlert: {
        value: false
      }
    };
  }

  ready(){
    super.ready();
    this.user= JSON.parse(window.atob(sessionStorage.getItem('user')));
    let user = this.user;
    this.lastAccess = (this.user).lastAccess.substring(0,10) + " " + (this.user).lastAccess.substring(11,19);
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)',
    ];
  }

  _routePageChanged(page) {
    this.page = page || 'cards';

    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _pageChanged(page) {
    if(page==="virtualCard.html"){
        this.page = 'cards';
    } else {
      if(page==="cards" || page==="movements" || page==="recharge" || page==="store" || page==="logout"){
        this.refreshCards=(page==="cards");
        this.refreshMovs=(page==="movements");
        this.refreshRecharge=(page==="recharge");
        this.refreshStore=(page==="store");

        page =  "../my-"+page+"/my-"+page+".html"

      } else {
        this.page = 'cards';
        this.refreshCards=(page==="cards");
      }

      const resolvedPageUrl = this.resolveUrl(page);
      Polymer.importHref(
          resolvedPageUrl,
          null,
          this._showPage404.bind(this),
          true);
    }
  }

  _pageInternalChanged(page){
    console.log(page);
  }

  _logout(){
    alert("logout");
  }

  _showPage404() {
    this.page = 'view404';
  }
}

window.customElements.define(myAppVirtualCard.is, myAppVirtualCard);

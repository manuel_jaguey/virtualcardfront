class MyMovements extends Polymer.Element {
  static get is() { return 'my-movements'; }

  static get properties() {
    return {
      refresh: {
        value: false,
        observer: '_refresh'
      },
      user: {
        type: Object
      },
      cards: {
        type: Array
      },
      cardsCombo: {
        type: Array
      },
      movements: {
        type: Array
      },
      urlBase: {
        type: String,
        value: ''
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      },
      showAlert:{
        value:false,
        notify: true
      }
    };
  }

  _refresh(){
    this.cardsCombo = [];
    this.$.cardsBox.value = '';
    this.movements = [];
    this.getCards();
  }

  getCards(){
    this.showSpinner=true;
    let props = this;
    let url = this.urlBase + "/card/v1/cards";
    let client = new XMLHttpRequest();

    client.open("GET", url, true);
    client.setRequestHeader('Content-type','application/json');
    client.setRequestHeader('session-id', sessionStorage.getItem('session-id'));

    client.onreadystatechange = function() {
      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        props.cards = JSON.parse(client.responseText);
        let cardsCombo = [];

        if(props.cards.length==0){
            props.addEmptyMovement("No tienes tarjetas a consultar");
        } else {
          for(let i=0; i<props.cards.length; i++){
            let item = {};
            let nameCard = (props.cards[i].alias!=undefined && props.cards[i].alias.trim()!="") ? props.cards[i].alias : props.cards[i].product;
            let numberCard = "****" + (props.cards[i].number).substring(12,16);
            item.value= numberCard +" - "+ nameCard;
            item.label= numberCard +" - "+ nameCard;
            cardsCombo.push(item);
          }
        }

        props.cardsCombo = cardsCombo
        props.showSpinner=false;

      } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
        let error = JSON.parse(client.responseText);
        props._handleError(props, error.message, error.code);

      } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
        console.log(JSON.stringify(client.responseText));
        props.showSpinner=false;

      }
    }
    client.send();
  }

  change(comboCards){
    if(comboCards.target._focusedIndex>=0){
      this.showSpinner=true;
      let card = this.cards[comboCards.target._focusedIndex];
      this.getMovements(card);
    } else {
      this.movements=[];
    }
  }

  getMovements(card){
    let props = this;
    let url = this.urlBase + "/card/v1/cards/" + card.id +"/movements";
    let client = new XMLHttpRequest();

    client.open("GET", url, true);
    client.setRequestHeader('Content-type','application/json');
    client.setRequestHeader('session-id', sessionStorage.getItem('session-id'));

    client.onreadystatechange = function() {
      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        props.movements = JSON.parse(client.responseText);
        props.formatMovements(props);
        props.showSpinner=false;

      } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
        let error = JSON.parse(client.responseText);
        props.addEmptyMovement(error.message);
        props._handleError(props, error.message, error.code);


      } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
        let error = JSON.parse(client.responseText);
        props.addEmptyMovement(error.message);
        props.showSpinner=false;

      }
    }
    client.send();
  }

  formatMovements(props){
    if(props.movements.length==0){
      props.addEmptyMovement("Sin movimientos a listar");

    } else {
      for(let i=0; i<props.movements.length; i++){
          let amount = "$"+ props.numberFormat(props.movements[i].localAmount.amount,2);
          props.movements[i].localAmount.amount = amount;

          let operationDate = props.movements[i].operationDate;

          props.movements[i].operationDate = operationDate.substring(0,10);
          props.movements[i].operationHour = operationDate.substring(11,19);
      }
    }
  }

  numberFormat (amount, decimals) {
    amount += '';
    amount = parseFloat(amount.replace(/[^0-9\.]/g, ''));

    decimals = decimals || 0;
    if (isNaN(amount) || amount === 0)
        return parseFloat(0).toFixed(decimals);
    amount = '' + amount.toFixed(decimals);

    var amount_parts = amount.split('.'),
        regexp = /(\d+)(\d{3})/;

    while (regexp.test(amount_parts[0]))
        amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

    return amount_parts.join('.');
  }

  addEmptyMovement (label){
    this.movements = [];
    let emptyMovement = {};
    emptyMovement.localAmount = {};
    emptyMovement.localAmount.amount = "";
    emptyMovement.operationDate = "";
    emptyMovement.operationHour = "";
    emptyMovement.concept = label;
    this.movements.push(emptyMovement);
  }

  _handleError (props, messageResponse, codeResponse){
    props.messageError = (messageResponse!=undefined) ? messageResponse : props.messageError;
    props.showMessage = true;
    props.showSpinner=false;

    if(codeResponse.includes("SDS")){
      props.showAlert=true;
    }
  }

}

window.customElements.define(MyMovements.is, MyMovements);

class MyLogout extends Polymer.Element {
  static get is() { return 'my-logout'; }

  static get properties() {
    return {
      user: {
        type: Object
      },
      urlBase: {
        type: String,
        value: ''
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      }
    };
  }

  ready(){
      super.ready();
  }

  _logout(){
    this.showSpinner=true;
    this.user= JSON.parse(window.atob(sessionStorage.getItem('user')));

    let props = this;

    let url = this.urlBase + "/user/v1/user/" +this.user.idUser+ "/logout";
    let client = new XMLHttpRequest();

    client.open("GET", url, true);
    client.setRequestHeader('Content-type','application/json');
    client.setRequestHeader('session-id', sessionStorage.getItem('session-id'));

    client.onreadystatechange = function() {
      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        props.showSpinner=false;
        window.location.href = "index.html";

      } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
        props.showSpinner=false;
        window.location.href = "index.html";

      } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
        props.showSpinner=false;
        window.location.href = "index.html";

      }
    }
    client.send();
  }
}

window.customElements.define(MyLogout.is, MyLogout);

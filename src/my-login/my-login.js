class MyLogin extends Polymer.Element {
  static get is() { return 'my-login'; }

  static get properties() {
    return {
      username: {
        type: String,
        value: ''
      },
      password: {
        type: String,
        value: ''
      },
      urlBase: {
        type: String,
        value: ''
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      }
    };
  }

  ready(){
    super.ready();
    try {
			sessionStorage.removeItem('session-id');
      sessionStorage.removeItem('user');
		} catch (e) {
    }
  }

  _submitLogin (){
    this.showMessage=false;

    if(this.$.formLogin.validate()){
      this.showSpinner=true;
      let props = this;

      let url = this.urlBase + "/user/v1/user/" + window.btoa(this.username) + "/login";
      let client = new XMLHttpRequest();
      let body = this._getBody();

      client.open("POST", url, true);
      client.setRequestHeader('Content-type','application/json');

      client.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
          sessionStorage.setItem("session-id", client.getResponseHeader("session-id"));
          sessionStorage.setItem("user",window.btoa(client.responseText));
          window.location.href = "virtualCard.html";
          props.showSpinner=false;

        } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
          let error = JSON.parse(client.responseText);
          props._handleError(props, error.message, error.code);

        } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
          props._handleError(props);
        }
      }
      client.send(JSON.stringify(body));
    }
  }

  _getBody (){
    let passwd = window.btoa(this.password);
    let body = {};
    body.password = passwd;
    return body;
  }

  _handleError (props, messageResponse, codeResponse){
    props.messageError = (messageResponse!=undefined) ? messageResponse : props.messageError;
    props.showMessage = true;
    props.showSpinner=false;
  }
}

window.customElements.define(MyLogin.is, MyLogin);

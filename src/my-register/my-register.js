class MyRegister extends Polymer.Element {
  static get is() { return 'my-register'; }

  static get properties() {
    return {
      showRegister: {
        value: true
      },
      showSuccesRegister: {
        value: false
      },
      firtsName: {
        type: String,
        value: ''
      },
      fatherSurname: {
        type: String,
        value: ''
      },
      motherSurname: {
        type: String,
        value: ''
      },
      birthDate: {
        type: String,
        value: ''
      },
      email: {
        type: String,
        value: ''
      },
      password: {
        type: String,
        value: ''
      },
      confirmPassword: {
        type: String,
        value: ''
      },
      urlBase: {
        type: String,
        value: ''
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      }
    };
  }


  _submitRegister (){

    if(this.password != this.confirmPassword){
      this._handleError(this, "las contraseñas no coinciden");
    } else {
      this.showMessage=false;

      if(this.$.formRegister.validate()){
        this.showSpinner=true;
        let props = this;

        let url = this.urlBase + "/user/v1/user/";
        let client = new XMLHttpRequest();
        let body = this._getBody();

        client.open("POST", url, true);
        client.setRequestHeader('Content-type','application/json');

        client.onreadystatechange = function() {
          if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            props.showRegister=false;
            props.showSuccesRegister=true;
            props.showSpinner=false;

          } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
            let error = JSON.parse(client.responseText);
            props._handleError(props, error.message, error.code);

          } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
            props._handleError(props);
          }
        }
        client.send(JSON.stringify(body));
      }
    }
  }

  _getBody (){
    let name = {};
    name.firtsName = this.firtsName;
    name.fatherSurname = this.fatherSurname;
    name.motherSurname = this.motherSurname;
    let birthDate = this.birthDate;
    let email = this.email;
    let password = window.btoa(this.password);

    let body = {};
    body.name = name;
    body.birthDate = birthDate;
    body.email = email;
    body.password = password;

    return body;
  }

  _handleError (props, messageResponse, codeResponse){
    props.messageError = (messageResponse!=undefined) ? messageResponse : props.messageError;
    props.showMessage = true;
    props.showSpinner=false;
  }
}

window.customElements.define(MyRegister.is, MyRegister);

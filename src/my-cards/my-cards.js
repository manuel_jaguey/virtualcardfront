class MyCards extends Polymer.Element {
  static get is() { return 'my-cards'; }

  static get properties() {
    return {
      refresh: {
        value: false,
        observer: '_refresh'
      },
      user: {
        type: Object
      },
      cards: {
        type: Array
      },
      urlBase: {
        type: String,
        value: ''
      },
      internalPage: {
        type: String,
        value: '',
        notify: true
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      },
      showFormNew:{
        value: false
      },
      allowAddNew: {
        value: false
      },
      mobileCard: {
        type: String,
        value: ''
      },
      aliasCard: {
        type: String,
        value: ''
      },
      showSuccesAddNew:{
        value:false
      },
      showAlert:{
        value:false,
        notify: true
      }
    };
  }

  ready(){
    super.ready();
  }

  _refresh(){
    if(this.refresh==true){
      this.refresh=false;
      this.showMessage=false;
      this.showFormNew=false;
      this.showSuccesAddNew=false;
      this.cards=[];
      this.getCards();
    }
  }

  getCards(){
    this.showSpinner=true;
    let props = this;
    let url = this.urlBase + "/card/v1/cards";
    let client = new XMLHttpRequest();

    client.open("GET", url, true);
    client.setRequestHeader('Content-type','application/json');
    client.setRequestHeader('session-id', sessionStorage.getItem('session-id'));

    client.onreadystatechange = function() {
      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        props.cards = JSON.parse(client.responseText);
        props.allowAddNew = (props.cards.length<3);
        props.showSpinner=false;

      } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
        console.log(JSON.stringify(client.responseText));
        props.showSpinner=false;

      } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
        console.log(JSON.stringify(client.responseText));
        props.showSpinner=false;

      }
    }
    client.send();
  }

  _cancel(){
    this.showFormNew=false;
    this.refresh=false;
    this.showMessage=false;
    this.showSuccesAddNew=false;
    this.mobileCard=""
    this.aliasCard="";
    this.cards=[];
    this.getCards();
  }

  _submitCreateCard(){
    this.showMessage=false;

    if(this.$.formAddCard.validate()){

      this.showSpinner=true;
      let props = this;

      let card = this.cards[this.cardSelected];
      let url = this.urlBase + "/card/v1/card/";
      let client = new XMLHttpRequest();
      let body = this._getBody();

      client.open("POST", url, true);
      client.setRequestHeader('Content-type','application/json');
      client.setRequestHeader('session-id', sessionStorage.getItem('session-id'));

      client.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
          props.showSuccesAddNew=true;
          props.showSpinner=false;

        } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
          let error = JSON.parse(client.responseText);
          props._handleError(props, error.message, error.code);

        } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
          props._handleError(props);
        }
      }

      client.send(JSON.stringify(body));
    }
  }

  _getBody (){
    let body = {};
    body.mobile=this.mobileCard;
    body.alias=this.aliasCard
    body.imgCard="";
    return body;
  }

  _handleError (props, messageResponse, codeResponse){
    props.messageError = (messageResponse!=undefined) ? messageResponse : props.messageError;
    props.showMessage = true;
    props.showSpinner=false;

    if(codeResponse.includes("SDS")){
      props.showAlert=true;
    }
  }

  addCard (){
    this.showFormNew=true;
  }

}

window.customElements.define(MyCards.is, MyCards);

class MyCard extends Polymer.Element {
  static get is() { return 'my-card'; }

  static get properties() {
    return {
      user: {
        type: Object
      },
      card: {
        type: Object
      },
      urlBase: {
        type: String,
        value: ''
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      },
      nameClient: {
        type: String
      },
      amount: {
        type: String
      },
      numberBlock1: {
        type: String
      },
      numberBlock2: {
        type: String
      },
      numberBlock3: {
        type: String
      },
      numberBlock4: {
        type: String
      },
      cardName: {
        type: String,
        value: 'Tarjeta virtual'
      },
      expirationDate: {
        type: String,
        value: ''
      }
    };
  }

  ready(){
    super.ready();
    this.buildCard();
  }

  buildCard(){
    this.user= JSON.parse(window.atob(sessionStorage.getItem('user')));

    this.getNameClient();
    this.buildCardNumber();
    this.getAmount();
    this.getCardName();
    this.getExpirationDate();
  }

  getNameClient (){
    this.nameClient =  this.user.name.firtsName +" "+ this.user.name.fatherSurname;

  }

  buildCardNumber(){
    this.numberBlock1="· · · ·";
    this.numberBlock2="· · · ·";
    this.numberBlock3="· · · ·";
    this.numberBlock4= (this.card.number).substring(12,16);
  }

  getAmount(){
    this.amount = "$ "+ (parseFloat(this.card.currenBalace.amount)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
  }

  getCardName(){
    this.cardName = (this.card.alias!=undefined && this.card.alias.trim()!="") ? this.card.alias : this.cardName;
  }

  getExpirationDate(){
    this.expirationDate = this.card.expirationDate.substring(0,2) +"/"+ this.card.expirationDate.substring(2,4);
  }
}

window.customElements.define(MyCard.is, MyCard);

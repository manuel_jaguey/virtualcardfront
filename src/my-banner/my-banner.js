class MyBanner extends Polymer.Element {
  static get is() { return 'my-banner'; }

  static get properties() {
    return {
      takeOn: {
        value: false
      },
      topUp: {
        value: false
      },
      purchase: {
        value: false
      },
      step: {
        type: String,
        value: ""
      },
      message: {
        type: String,
        value: ""
      }
    };
  }

  ready(){
    super.ready();
  }

  register(){
    window.location.href = "registro.html";
  }
}

window.customElements.define(MyBanner.is, MyBanner);

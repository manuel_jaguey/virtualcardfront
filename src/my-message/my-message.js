class MyMessage extends Polymer.Element {
  static get is() { return 'my-message'; }

  static get properties() {
    return {
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      }
    };
  }
}
window.customElements.define(MyMessage.is, MyMessage);

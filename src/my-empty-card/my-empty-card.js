class myEmptyCard extends Polymer.Element {
  static get is() { return 'my-empty-card'; }

  static get properties() {
    return {
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      }
    };
  }

  ready(){
      super.ready();
  }

}

window.customElements.define(myEmptyCard.is, myEmptyCard);

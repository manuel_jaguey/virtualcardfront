class MyRecharge extends Polymer.Element {
  static get is() { return 'my-recharge'; }

  static get properties() {
    return {
      refresh: {
        value: false,
        observer: '_refresh'
      },
      user: {
        type: Object
      },
      cards: {
        type: Array
      },
      cardsCombo: {
        type: Array
      },
      movements: {
        type: Array
      },
      urlBase: {
        type: String,
        value: ''
      },
      messageError: {
        type: String,
        value: 'Servicio temporalmente no disponible'
      },
      showMessage: {
        value: false
      },
      showSpinner: {
        value: false
      },

      nameOwner: {
        type: String,
      },
      creditCard: {
        type: String,
      },
      codeCvv: {
        type: String,
      },
      expiration: {
        type: String,
      },
      expirationMonth: {
        type: String,
      },
      expirationYear: {
        type: String,
      },
      amountRecharge: {
        type: String,
      },
      cardSelected: {
        type: Number,
        value: -1
      },
      succesTopUp: {
        value: false
      },
      showAlert:{
        value:false,
        notify: true
      }
    };
  }

  _refresh(){
    if(this.refresh==true){
      this.clean();
      this.getCards();
    }
  }

  getCards(){
    this.showSpinner=true;
    let props = this;
    let url = this.urlBase + "/card/v1/cards";
    let client = new XMLHttpRequest();

    client.open("GET", url, true);
    client.setRequestHeader('Content-type','application/json');
    client.setRequestHeader('session-id', sessionStorage.getItem('session-id'));

    client.onreadystatechange = function() {
      if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
        props.cards = JSON.parse(client.responseText);
        let cardsCombo = [];

        if(props.cards.length==0){
          props._handleError(props, "No tienes tarjetas a consultar", "WSGN001");

        } else {
          for(let i=0; i<props.cards.length; i++){
            let item = {};
            let nameCard = (props.cards[i].alias!=undefined && props.cards[i].alias.trim()!="") ? props.cards[i].alias : props.cards[i].product;
            let numberCard = "****" + (props.cards[i].number).substring(12,16);
            item.value= numberCard +" - "+ nameCard;
            item.label= numberCard +" - "+ nameCard;
            cardsCombo.push(item);
          }
        }

        props.cardsCombo = cardsCombo
        props.succesTopUp=false;
        props.showSpinner=false;


      } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
        let error = JSON.parse(client.responseText);
        props._handleError(props, error.message, error.code);

      } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
        console.log(JSON.stringify(client.responseText));
        props.showSpinner=false;

      }
    }
    client.send();
  }

  _submitTopUp(){
    this.showMessage=false;

    if(this.$.formTopUp.validate()){

      if(this.cardSelected>=0){
        this.showSpinner=true;
        let props = this;

        let card = this.cards[this.cardSelected];
        let url = this.urlBase + "/card/v1/cards/" + card.id +"/topUp";
        let client = new XMLHttpRequest();
        let body = this._getBody();

        client.open("PUT", url, true);
        client.setRequestHeader('Content-type','application/json');
        client.setRequestHeader('session-id', sessionStorage.getItem('session-id'));

        client.onreadystatechange = function() {
          if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            props.succesTopUp=true;
            props.showSpinner=false;

          } else if (this.readyState === XMLHttpRequest.DONE && this.status === 400) {
            let error = JSON.parse(client.responseText);
            props._handleError(props, error.message, error.code);

          } else if (this.readyState === XMLHttpRequest.DONE && (this.status === 500 || this.status === 0)) {
            props._handleError(props);
          }
        }

        client.send(JSON.stringify(body));

      } else {
        this._handleError(this, "Selecciona una tarjeta", "WSG001");
      }
    }
  }

  _getBody (){
    let body = {};

    body.numberCard = window.btoa(this.creditCard);
    body.code = window.btoa(this.codeCvv);
    body.expiration = window.btoa(this.expirationMonth+""+this.expirationYear);
    body.amount = this.amountRecharge;
    body.nameOwner = this.nameOwner;
    return body;
  }

  _handleError (props, messageResponse, codeResponse){
    props.messageError = (messageResponse!=undefined) ? messageResponse : props.messageError;
    props.showMessage = true;
    props.showSpinner=false;

    if(codeResponse.includes("SDS")){
      props.showAlert=true;
    }
  }

  change(comboCards){
    this.cardSelected=comboCards.target._focusedIndex;
  }

  clean (){
    this.showMessage = false;
    this.succesTopUp=false;
    this.creditCard="";
    this.codeCvv="";
    this.expirationMonth="";
    this.expirationYear="";
    this.amountRecharge="";
    this.nameOwner="";

  }

}

window.customElements.define(MyRecharge.is, MyRecharge);

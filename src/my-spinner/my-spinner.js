class MySpinner extends Polymer.Element {
  static get is() { return 'my-spinner'; }

  static get properties() {
    return {
      showSpinner: {
        value: false
      }
    };
  }
}
window.customElements.define(MySpinner.is, MySpinner);

class MyAlert extends Polymer.Element {
  static get is() { return 'my-alert'; }

  static get properties() {
    return {
      showAlert: {
        value: false
      },
      messageAlert: {
        type: "String",
        value: "Tu sesión ha finalizado",
        notify: true
      }
    };
  }

  _acept (){
    let page="cards";

    window.location.href="/index.html";
  }

  _showPage404() {
    this.page = 'view404';
  }
}
window.customElements.define(MyAlert.is, MyAlert);
